# TEMENT

TEMENT software (Tool for Topic and Named Entity Analysis), free open source software (GNU license), designed for the analysis of text corpora centered on the identification of topics and named entities. The processing chain is a suite of R and Python scripts that collects, processes and visualizes the results of Topic Modeling (Latent Dirichlet Allocation) on text corpora from the press and digital social networks.

The software was developed as part of the TEREM project, granted by Labex DRIIHM and IGlobes ( CNRS- University of Arizona) and  the MEDIA4HEALTH project, granted by Institut ISSPAM, Aix-Marseille University. These projects are  coordinated by **Brigitte Juanals**, professor at Aix-Marseille University. The software components are deposited in the Huma-Num Gitlab of the Huma-Num IR* to enable public access and reuse.

## R scripts  run in the following order:

ParseEuropresse.R, which processes HTML files collected on the Europresse platform

Lemmatize.R, which lemmatizes the texts of press articles for processing by the script that calculates the breakdown by Topic

TidyCategory.R, which visualizes the monthly flow of press articles by category

ComputeAndVisualizeTopic.R calculates Topics, indexes texts with Topics and visualizes annual variations.

##  Python scripts run  in the following order:

NerInTexts.py, which identifies and categorizes named entities in texts

BuildNodesAndEdges.py, which produces nodes and edges files that can be used by the Gephi software.


# PUBLICATIONS

Brigitte Juanals, Jean-Luc Minel. A Comparative Analysis of Long Covid in the French Press and Twitter. Communications in Computer and Information Science, 2023, Advances in Computational Collective Intelligence, Springer, 1864 (1), pp.379-392. ⟨10.1007/978-3-031-41774-0⟩. ⟨halshs-04214758⟩
