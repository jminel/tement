# -*- coding: utf-8 -*-
"""
Created on Thu Nov  12 10:18:54 2021

@author: modyco https://www.stat4decision.com/fr/traitement-langage-naturel-francais-tal-nlp/
https://spacy.io/usage/linguistic-features#named-entities
Identifying NER in newspapers
text format (csv)
doc_id;idText;date;mois;year;journal;catego;text
"""

import spacy
import csv
import time

def return_token(sentence):
    # Tokeniser la phrase
    doc = nlp(sentence)
    # Retourner le texte de chaque token
    return [X.text for X in doc]

def return_NER(sentence):
    # Tokeniser la phrase
    doc = nlp(sentence)
    # Retourner le texte et le label pour chaque entite
    return [(X.text, X.label_) for X in doc.ents]

# checking a the list of joural and modifying it
def checkListJournal(listJournal,journal) :
    findIt = False
    listOnlyJournal =listJournal[1]
    for elem in listOnlyJournal :
        if (str(elem[0]) == str(journal)) :
            elem[1] = elem[1] + 1
            findIt = True
            break              
    if (findIt != True) :
        listOnlyJournal.append([journal,1])
    listJournal = [listJournal[0],listOnlyJournal]
    return listJournal
        
           
            
            
            

nlp = spacy.load("fr_core_news_lg")
#nlp = spacy.load("en_core_web_sm")
# reading files from directory containning txm files
pathdir = '.../DataHTML/'
metadatafile = pathdir + 'EuropresseForTopic.csv'
dicoNER = {}   # key is the NER
dicoJournal ={}  # key is Journal
dicoNERbyYear = {}
print("processing file")
with open(metadatafile, 'r',  encoding='utf-8') as f:
        # reading first line, ingnoring (headings)
        line =f.readline()
        # reading line by line
        for line in f :
            list=line.split(';')
            idText = list[0] 
            year = int(list[3])
            # print(idText)
            journal = list[4]
            journal = journal.replace(",", " ")
            #☺ counting journal with NER
            if (journal in dicoJournal) :
                pass
            else :
                dicoJournal[journal] = 1
                
            texteContent= list[6]
           #  to help spacy
            texteContent = texteContent.replace("-"," ")
#           identifying NER
            nertest = return_NER(texteContent)
            for tupleNER in nertest :
                # checking if already meet this NER
                nerClean=tupleNER[0].strip()
                nerClean = nerClean.replace(","," ")
                if (nerClean in dicoNER ) :
                    # yes moddifying the count in list of journal
                    listJournal = dicoNER[nerClean]
                    listModify = checkListJournal(listJournal,journal)
                    dicoNER[nerClean] = listModify 
                else :   #first time we keep also he type of he NER in the list
                    # adding only NER composed at least 2 characters and less than 20
                    if ( (len(nerClean) > 2) and (len(nerClean) < 20) ) :
                        dicoNER[nerClean] = [tupleNER[1], [[journal, 1]]]
                        
                # dicoNerbyYear
                if (nerClean in dicoNERbyYear) :
                    bloc = dicoNERbyYear[nerClean]
                    blocyear= bloc[1]
                    if (year == 2020):
                        blocyear[0][1] =  blocyear[0][1]  +1
                    if (year == 2021) :
                        blocyear[1][1] = blocyear[1][1] +1
                    if (year == 2022 ):
                        blocyear[2][1] = blocyear[2][1]  +1
                    newbloc = [bloc[0], blocyear]
                    dicoNERbyYear[nerClean] = newbloc
    
    
                else :
                    if ( (len(nerClean) > 2) and (len(nerClean) < 20) ) :
                        bloclist = [[2020,0], [2021,0], [2022,0]]
                        if (year == 2020) :
                            bloclist[0][1] = 1
                        if (year == 2021) :
                            bloclist[1][1] = 1
                        if (year == 2022) :
                            bloclist[2][1] = 1                           
                        dicoNERbyYear[nerClean] = [tupleNER[1], bloclist]
                    else :
                        pass
            
                
f.close()
print ("nombre d entite", len(dicoNER))
print ("nombre journal", len(dicoJournal))
  # writing csv file, NER, typeOfNer, Journal, value   
print("writing NER by source")
nerfile = pathdir + 'ner2021.csv' 
with open(nerfile, 'w', newline='', encoding='utf-8') as f :
    out = csv.writer(f, delimiter = ',' )
    row = ["Entite", "Type", "Source", "Occurrence" ]
    out.writerow(row)

    for clef, value in dicoNER.items() :
        typeNer = value[0]
        for elem in value[1] :
            journal = elem[0]
            freq = elem[1]
            row = [clef, typeNer, journal,freq]
            out.writerow(row)

f.close()    
# Sorting by entity only
print("writing NER by frequency")
nerfile = pathdir + 'nerFreq.csv' 
with open(nerfile, 'w', newline='', encoding='utf-8') as f :
    out = csv.writer(f, delimiter = ',' )
    row = ["Entite", "Occurrence" ]
    out.writerow(row)
    dicoOnlyNer = {}
    for clef, value in dicoNER.items() :
        typeNer = value[0]
        for elem in value[1] :
            freq = elem[1]
           # print(clef, freq)
            if (clef in dicoOnlyNer) :
                #existing adding frequency
                dicoOnlyNer[clef] = dicoOnlyNer[clef] + freq            
            else :
                # new one
                dicoOnlyNer[clef] =  freq   
                
    for clef, value  in  dicoOnlyNer.items():
        row = [clef,value]
        out.writerow(row)
f.close()  

print("writing NER by Year")  
print("Done")   
nerfile = pathdir + 'nerbyYear.csv' 
with open(nerfile, 'w', newline='', encoding='utf-8') as f :
    out = csv.writer(f, delimiter = ';' )  
    row = ["Entite","Type",  "Year", "Occurrence"]
    out.writerow(row)
    for clef, value in dicoNERbyYear.items() :
        if ( (value[0] == "PER") | (value[0] == "ORG" ) ) :
            blocyear = value[1]
            row = [clef, value[0],blocyear[0][0],blocyear[0][1] ]
            out.writerow(row)
            row = [clef, value[0],blocyear[1][0],blocyear[1][1] ]
            out.writerow(row)
            row = [clef, value[0],blocyear[2][0],blocyear[2][1] ]
            out.writerow(row)
f.close()  
        
        
           
