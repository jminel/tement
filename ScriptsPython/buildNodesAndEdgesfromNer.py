# -*- coding: utf-8 -*-
"""
Created on Thu Nov  12 10:18:54 2021

@author: modyco https://www.stat4decision.com/fr/traitement-langage-naturel-francais-tal-nlp/
https://spacy.io/usage/linguistic-features#named-entities
Identifying NER in newspapers
text format (csv)
doc_id;idText;date;mois;year;journal;catego;text
"""


import csv
import time


pathdir = '..../CovidLongTextes/'
metadatafile = pathdir + 'ner2021FreqperMultiSources.csv'
# Entite	Type	Source	OccurrenceInSource, OccurrenceAll

dicoNER = {}   # key is the NER
dicoJournal ={}  # key is Journal
print("processing file")
with open(metadatafile, 'r',  encoding='utf-8') as f:
        # reading first line, ingnoring (headings)
        line =f.readline()
        # reading line by line, storing lines in a dictionary, clef is meaningless
        clef = 1
        for line in f :
           # print(line)
            list=line.split(',')
            entity = list[0] 
            typeEntity = list[1]
            # print(idText)
            journal = list[2]
            freqInSource = int(list[3])
            freqAll = int(list[4])
            nerClean=entity.strip()
            value = [nerClean, typeEntity, journal,freqInSource, freqAll]
            dicoNER[clef] = value 
            clef = clef +1
             
           
f.close()
print ("nombre d entite", len(dicoNER))
  
#  building files nodes and edges for Gephi
#building Id for all nodes : entity and journal
dicoNodes={}
identity = 1
for clef, value in dicoNER.items() :
    # giving Id at entity
    if (value[0] in dicoNodes) :
        pass
    else:
        dicoNodes[value[0]] = identity
        identity = identity +1
    # giving Id at Journal
    journal = value[2]
    if (journal in dicoNodes) :
        pass
    else :
        dicoNodes[journal] = identity
        identity = identity + 1
# writing nodes
            
print("writing Nodes ")
nerfile = pathdir + 'nodes.csv' 
listeEdges = []
dicoNodesProcessed= {}
for clef, value in dicoNER.items() :
    # print(clef, value)
    typeNer = value[1]
    journal = value[2]
    freqInSource = int(value[3])
    freqAll = int(value[4])
    IdEntity = dicoNodes[value[0]]
    IdJournal = dicoNodes[journal]
    label = value[0]

    if (IdEntity not in dicoNodesProcessed) :
        value = [IdEntity, label, typeNer, freqAll]
        dicoNodesProcessed[IdEntity]= value  
    else :
        pass
           
    # building list edges
            

    edge = [IdJournal, IdEntity, "Directed"]
    listeEdges.append(edge)

            # adding journal at Nodes files

    freqAll = "1" #not useful but manadatory to be compliant with entity node
    if (IdJournal not in dicoNodesProcessed ) :
        value = [IdJournal, journal, "JOU",freqAll]
        dicoNodesProcessed[IdJournal]= value


print("writing nodes")
with open(nerfile, 'w', newline='', encoding='utf-8') as f :
    out = csv.writer(f, delimiter = ',' )
    row = ["Id","Label","Type", "OccurenceAll"]
    out.writerow(row)  
    for clef, value in dicoNodesProcessed.items() :
        row = [value[0], value[1], value[2], value[3] ]
        out.writerow(row)   
f.close()

print("writing edges")
nerfile = pathdir + 'edges.csv' 
with open(nerfile, 'w', newline='', encoding='utf-8') as f :
    out = csv.writer(f, delimiter = ',' )
    row = ["Source","Target","Type"]
    out.writerow(row)  
    for elem in listeEdges :
        row = elem
        out.writerow(row)   
f.close()

print("Done")

          
