# see  https://quanti.hypotheses.org/1416

setwd(dir = "") # Working directory

load.lib <- c("xml2", "XML", "stringr", "stringdist", "stringi","lubridate", "dplyr", "tidyr", "ggplot2", "glue") # package needed
install.lib <- load.lib[!load.lib %in% installed.packages()] # checking installed packages

for (lib in install.lib) install.packages(lib,dependencies=TRUE) # installing missing packages

sapply(load.lib,require,character=TRUE) # loading packages

LIRE <- function(html) {
  doc <- htmlParse(html) # parsing thtml document 
  articles <- getNodeSet(doc, "//article") # get article 
  
  journal <- sapply(articles, function(art) {
    journ <- xpathSApply(art, "./header/div[@class='rdp__DocPublicationName']/span[@class='DocPublicationName']/text()", xmlValue)
    journ <-journ[[1]]
    if (is.null(journ)) journ <- NA
    journ
  })
  
  auteur <- sapply(articles, function(art) { # get author
    aut <- xpathSApply(art, "./header/div[@class='docAuthors']/text()", xmlValue)
   if (is.null(aut)) { aut <- "Anonyme"}
    else { aut<- as.character(aut) }
    aut
    
  })
  
  titre <- sapply(articles, function(art) { # get title
    tmp <- xpathSApply(art, "./header/div[@class='titreArticle']//text()", xmlValue)
    if (is.null(tmp)) tmp <- NA
    tmp <- paste(tmp, collapse = "") 
    str_trim(tmp)
  })
  
  date <- sapply(articles, function(art) { # get date
    tmp <- xpathSApply(art, ".//div[@class='publiC-lblNodoc']//text()", xmlValue)
    if (is.null(tmp)) tmp <- NA
    tmp <- substr(tmp, 6, 13)
    tmp
  })
# adding as.character(date) to convert as string before calling Date
  date <- as.Date(as.character(date), "%Y%m%d") # normalizing date
  
  texte <- sapply(articles, function(art) { # get text of article
    tmp <- xpathSApply(art, ".//div[@class='DocText clearfix']//text()", xmlValue)
    if (is.null(tmp)) tmp <- NA
    tmp <- paste(tmp, collapse = "")
    str_trim(tmp)
  })
  lengthTexte <- str_length(texte)


  # put metadata and text in a data.frame
  txt <- try(data.frame(Journal = journal,
                    Titre = titre,
                    Auteur = auteur,
                    Date = date,
                    LengthTexte = lengthTexte,
                     Texte = texte)
  )
  
  
  # deleting  NA lines 
  txt <- subset(txt, !is.na(Journal) & !is.na(Titre))
  txt
  
}
# reading HTML files
lire_dossier <- function(chemin) {
  
  list<-list.files(chemin, pattern= ".HTML", full.names=TRUE, recursive=TRUE)
  
  l <- lapply(list, function(file) {
    print(file)
    LIRE(html=file)
  })
  bind_rows(l)
}
pathToHTMLDirectory = " "
test <- lire_dossier(pathToHTMLDirectory ) 
# writing csv file
csvFile = " " 
fivleutf8 <- file(csvFile, encoding ="UTF-8"  )
write.csv2(test, file=fileutf8, row.names = FALSE) 
print("Done *******************")

# reading csv file
#  normalizing  title
tuto <-read.csv2(csvFile,stringsAsFactors = F)
tuto$Journal[stri_detect_fixed(tuto$Journal, "AFP",case_insensitive=T)] <- "AFP"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Atlantico",case_insensitive=T)] <- "Atlantico"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Aisne",case_insensitive=T)] <- "Aisne_Nouvelle"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Challenges",case_insensitive=T)] <- "Challenges"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Closer",case_insensitive=T)] <- "Closer"
tuto$Journal[stri_detect_fixed(tuto$Journal, " HuffPost ",case_insensitive=T)] <- "HuffPost"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Courrier international",case_insensitive=T)] <- "Courrier_international"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Echos",case_insensitive=T)] <- "Les_Echos"
tuto$Journal[stri_detect_fixed(tuto$Journal, "figaro",case_insensitive=T)] <- "Le_Figaro"
tuto$Journal[stri_detect_fixed(tuto$Journal, "monde",case_insensitive=T)] <- "Le_Monde"
tuto$Journal[stri_detect_fixed(tuto$Journal, "libération",case_insensitive=T)] <- "Liberation"
tuto$Journal[stri_detect_fixed(tuto$Journal, "équipe",case_insensitive=T)] <- "Equipe"
tuto$Journal[stri_detect_fixed(tuto$Journal, "humanit?",case_insensitive=T)] <- "Humanite"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Croix",case_insensitive=T)] <- "La_Croix"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Tribune",case_insensitive=T)] <- "La_Tribune"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Berry",case_insensitive=T)] <- "Berry_republicain"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Aveyron",case_insensitive=T)] <- "Centre_Presse Aveyron"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Echo républicain",case_insensitive=T)] <- "Echo_Republicain"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Journal du Centre",case_insensitive=T)] <- "Journal_Centre"
tuto$Journal[stri_detect_fixed(tuto$Journal, "maine",case_insensitive=T)] <- "Le_Maine_Libre"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Point",case_insensitive=T)] <- "Le_Point"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Express",case_insensitive=T)] <- "Express"
tuto$Journal[stri_detect_fixed(tuto$Journal, "indépendant",case_insensitive=T)] <- "Independant"
tuto$Journal[stri_detect_fixed(tuto$Journal, "courrier de l'ouest",case_insensitive=T)] <- "Courrier_Ouest"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Est éclair",case_insensitive=T)] <- "Est_Eclair"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Est républicain",case_insensitive=T)] <- "Est_Republicain"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Obs",case_insensitive=T)] <- "Obs"
tuto$Journal[stri_detect_fixed(tuto$Journal, "la Vie",case_insensitive=T)] <- "La_Vie"
tuto$Journal[stri_detect_fixed(tuto$Journal, "télégramme",case_insensitive=T)] <- "Le_Telegramme"
tuto$Journal[stri_detect_fixed(tuto$Journal, "bleu",case_insensitive=T)] <- "Bleu_Agen"
tuto$Journal[stri_detect_fixed(tuto$Journal, "20 Minutes",case_insensitive=T)] <- "20_Minutes"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Aujourd'hui",case_insensitive=T)] <- "Aujourd_hui"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Montagne",case_insensitive=T)] <- "La_Montagne"
tuto$Journal[stri_detect_fixed(tuto$Journal, "marie",case_insensitive=T)] <- "Marie_France"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Quotidien",case_insensitive=T)] <- "Quotidien_Medecin"
tuto$Journal[stri_detect_fixed(tuto$Journal, "moniteur",case_insensitive=T)] <- "Le_Moniteur"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Midi Libre",case_insensitive=T)] <- "Midi_Libre"
tuto$Journal[stri_detect_fixed(tuto$Journal, "nice matin",case_insensitive=T)] <- "Nice_Matin"
tuto$Journal[stri_detect_fixed(tuto$Journal, "nouvelle r?publique",case_insensitive=T)] <- "Nouvelle_Republique"
tuto$Journal[stri_detect_fixed(tuto$Journal, "populaire",case_insensitive=T)] <- "Populaire Centre"
tuto$Journal[stri_detect_fixed(tuto$Journal, "république des pyrénées",case_insensitive=T)] <- "Republique_Pyrenees"
tuto$Journal[stri_detect_fixed(tuto$Journal, "république du centre",case_insensitive=T)] <- "Republique_Centre"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Ouest-France",case_insensitive=T)] <- "Ouest_France"
tuto$Journal[stri_detect_fixed(tuto$Journal, "normandie",case_insensitive=T)] <- "Paris_Normandie"
tuto$Journal[stri_detect_fixed(tuto$Journal, "match",case_insensitive=T)] <- "Paris_Match"
tuto$Journal[stri_detect_fixed(tuto$Journal, "roannais",case_insensitive=T)] <- "Pays_Roannais"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Pélerin",case_insensitive=T)] <- "Le_Pelerin"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Océan",case_insensitive=T)] <- "Presse_Ocean"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Nord Littoral",case_insensitive=T)] <- "Nord_Littoral"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Provence",case_insensitive=T)] <- "La_Provence"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Sant?+",case_insensitive=T)] <- "Sante_Magazine"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Science et vie",case_insensitive=T)] <- "Science_et_Vie"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Sciences et avenir",case_insensitive=T)] <- "Sciences_et_Avenir"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Sud Ouest",case_insensitive=T)] <- "Sud_Ouest"
tuto$Journal[stri_detect_fixed(tuto$Journal, "top santé",case_insensitive=T)] <- "Top_Sante"
tuto$Journal[stri_detect_fixed(tuto$Journal, "télérama",case_insensitive=T)] <- "Telerama"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Nord éclair",case_insensitive=T)] <- "Nord_Eclair"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Voix",case_insensitive=T)] <- "Voix_du_Nord"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Yahoo",case_insensitive=T)] <- "Yahoo"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Var",case_insensitive=T)] <- "Var_Matin"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Yonne",case_insensitive=T)] <- "Yonne_Republicaine"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Loire",case_insensitive=T)] <- "Saone_et_Loire"
tuto$Journal[stri_detect_fixed(tuto$Journal, "progrès",case_insensitive=T)] <- "Progres_Lyon"
tuto$Journal[stri_detect_fixed(tuto$Journal, "Centre-ouest",case_insensitive=T)] <- "Centre_Ouest"
tuto$Journal[stri_detect_fixed(tuto$Journal, "dèpêche",case_insensitive=T)] <- "Depeche_Midi"
tuto$Journal[stri_detect_fixed(tuto$Journal, "picard",case_insensitive=T)] <- "Courrier_Picard"
tuto$Journal[stri_detect_fixed(tuto$Journal, "charente",case_insensitive=T)] <- "Charente_Libre"
tuto$Journal[stri_detect_fixed(tuto$Journal, "corse",case_insensitive=T)] <- "Corse_Matin"
tuto$Journal[stri_detect_fixed(tuto$Journal, "dordogne",case_insensitive=T)] <- "Dordogne_Libre"
tuto$Journal[stri_detect_fixed(tuto$Journal, "union",case_insensitive=T)] <- "Union"
tuto$Journal[stri_detect_fixed(tuto$Journal, "valeurs",case_insensitive=T)] <- "Valeurs_Actuelles"
tuto$Journal[stri_detect_fixed(tuto$Journal, "internaute",case_insensitive=T)] <- "Internaute"

#excluding some sentences
tuto$Texte <- gsub("Cet article est paru dans", " " , tuto$Texte)
tuto$Texte <- gsub("Cet article a également été publié dans les éditions suivantes", " " , tuto$Texte)
tuto$Texte <- gsub("Voir article", " " , tuto$Texte)


# Excluding some journals and some short texts and bad code javascript

tutoClean <- filter(tuto,tuto$Journal != "Yahoo" & tuto$LengthTexte > 300 & 
                      str_detect(tuto$Texte, ".minitableau") == FALSE & 
                      str_detect(tuto$Texte, ".array") == FALSE &
                      str_detect(tuto$Texte, ".function") == FALSE &
                      str_detect(tuto$Journal, "AFP") == FALSE &    # AFP
                      str_detect(tuto$Journal, "Option Finance") == FALSE &      # Agence option finance
                      str_detect(tuto$Journal, "Boursier") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "Bulletin") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "Chef") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "DAF") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "Assurance") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "78actu") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "Actu.fr") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "Boursorama") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "Radio") == FALSE  & # Agence option finance
                      str_detect(tuto$Journal, "Yahoo") == FALSE   &
                      str_detect(tuto$Journal, "Good") == FALSE   & # Dr Good
                      str_detect(tuto$Journal, "Acteurs") == FALSE   &   # Acteurs publics
                      str_detect(tuto$Journal, "Industrie") == FALSE   &
                      str_detect(tuto$Journal, "Usine") == FALSE   &  #usine Nouvelle
                      str_detect(tuto$Journal, "Correspondance" ) == FALSE  & # Correspopndance economique 
                      str_detect(tuto$Journal, "Equipe" ) == FALSE   &
                      str_detect(tuto$Journal, "WK" ) == FALSE    
                       )                     

tutoClean$Catego <- "Autre"
# categorizing articles
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Echos")] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Figaro")] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Liberation")] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Monde")] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Croix")] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Equipe"  )] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Humanite")] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"La_Tribune")] <- "PQN"

tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Independant")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Berry")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Journal_du_Centre")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Maine")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Midi")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Nice")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Est")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Ouest_France")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Montagne")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Telegramme")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Ocean")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Union")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Sud_Ouest")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Nord_Eclair")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Var_Matin")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Yonne_Republicaine")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Voix")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Est_Republicain")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"La_Provence")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Saone_et_Loire")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Progres")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Centre_Ouest")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Courrier_Picard")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Charente_Libre")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Corse_Matin")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Dordogne_Libre")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Nouvelle_Republique")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Republique_Pyrenees")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Republique_Centre")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Paris_Normandie")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Bleu_Agen")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Aveyron")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Echo_Republicain")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Courrier Ouest")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Pays_Roannais")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Nord_Littoral")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Aisne_Nouvelle")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Populaire_Centre")] <- "PQR"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Depeche_Midi")] <- "PQR"

tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Atlantico")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Challenges")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"HuffPost")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Courrier international")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Point")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Express")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Obs")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Vie")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Moniteur")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Paris_Match")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Telerama")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Closer")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Science_et_Vie")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Sciences_et_Avenir")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Pelerin")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Marie_France")] <- "Hebdo"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Valeurs_Acuelles")] <- "Hebdo"


tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"20_Minutes")] <- "PQN"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Aujourd_hui")] <- "PQN"

tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Quotidien_Medecin")] <- "Sante"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Sante_Magazine")] <- "Sante"
tutoClean$Catego[stri_detect_fixed(tutoClean$Journal,"Top_Sante")] <- "Sante"

                 
tutoForTidy <- select(tutoClean, Journal, Date, Catego)  
csvFileClean = " "                
fileutf8Clean <- file(csvFileClean , encoding ="UTF-8" )
# writing csv clean file
write.csv2(tutoForTidy,file=fileutf8Clean, row.names = FALSE)
print("Done Cleaning *******************")


# transforming for Iramuteq *******************************************************
tutoForIramuteq <- select(tutoClean, Journal, Date, Catego, Texte) 
csvFileTmp = " " 
fileutf8Clean <- file(csvFileTmp, encoding ="UTF-8" )
# writing csv file 
write.csv2(tutoForIramuteq,file=fileutf8Clean, row.names = FALSE)

# rereading the csv file for Iramuteq
iram <-read.csv2(csvFileTmp , stringsAsFactors = F)

iram$Annee <- year(iram$Date)
iram_text<- select(iram, c(Annee, Journal, Date, Catego, Texte))
iram_text$Annee <- paste("**** *Annee_", iram_text$Annee, sep = "") # setting variables for Iramuteq
iram_text$Journal <- paste("*journal_", iram_text$Journal, sep = "")
iram_text$Catego <- paste("*catego_", iram_text$Catego, sep = "")
iram_text$Date <- paste("*date_", iram_text$Date, sep = "")
iram_text$Texte<- paste("\n",iram_text$Texte, "\n", sep=" ")
csvFileForIramuteq = ""
# writng csv file for Iramuteq
write.table(iram_text, file= csvFileForIramuteq , sep=" ", 
            quote=FALSE, row.names=FALSE, col.names=FALSE)



# transforming for TXM
TXM <-tutoClean
directoryForTXM = ""
setwd(dir = directoryForTXM) #
for (i in 1:length(TXM$Texte)) {
  x <- paste(directoryForTXM, 'T', i, ".txt", sep="")
  write.table(TXM$Texte[i], file=x, col.names=F, row.names=F, fileEncoding = "UTF-8")
}
TXM$Annee <- year(TXM$Date)
TXM$Mois <- month(TXM$Date)

TXM <- select (TXM, Journal,  Annee, Mois)
# adding identifier
TXM <- mutate(TXM, id= paste("T",rownames(TXM), sep = ""))

TXM<- TXM[,c("id","Journal", "Annee", "Mois")]
#adding header for the files

colnames(TXM) <- c("id", "Journal",  "Annee","Mois")

medatafile <- paste(directoryForTXM, "metadata.csv", sep="")
write.table(TXM, file=medatafile,
            col.names=T, row.names=F, sep=",", fileEncoding = "UTF-8")




